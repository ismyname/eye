#!/usr/bin/env python3


import os
import re
import json
import argparse
import subprocess


def run(command):
    process = subprocess.run(command,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
    return process.stdout.decode('utf-8')

def ldd_block_devices(model):
    payload = list()
    for entry in os.listdir('/sys/block'):
        try:
            with open(f'/sys/block/{entry}/device/model') as f:
                _model = f.read()
        except FileNotFoundError:
            continue
        if re.search(model, _model):
            payload.append({'{#NAME}': entry})
    return json.dumps(payload)

def nvme(device, keyword):
    output = run(['sudo', '/usr/sbin/nvme', 'smart-log', f'/dev/{device}'])
    result = re.findall('^(.*\w) *: *(.*\d).*$', output, re.M)
    dicted = {group[0]: group[1] for group in result}
    return dicted[keyword]

def sensor():
    output = run(['sensor'])
    result = re.findall('^Package id 0\D+(\d+).+$', output, re.M)
    return result

def zfs_get(keyword, pool):
    output = run(['zfs', 'get', '-H', '-o', 'value', '-p', keyword, pool])
    if keyword == 'available':
        return int(output) / 1024 / 1024 / 1024
    return output

def iops(device):
    output = run(['iostat', '2', '2', '-d', '/dev/' + device])
    values = []
    for line in output.splitlines():
        if device in line:
            values.append(line.split()[1])
    return values[1].replace(',', '.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--discover')
    parser.add_argument('--device')
    parser.add_argument('--keyword')
    parser.add_argument('bullshit', nargs='?')
    args = parser.parse_args()
    if args.device and args.keyword:
        print(nvme(args.device, args.keyword))
    if args.discover:
        print(ldd_block_devices(args.discover))
